---
title: "CB.con"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(pacman)
p_load(data.table,dplyr,dtplyr,effects,ggplot2,lmerTest,MuMIn)
p_load(tidyr)
p_load(rethinking)
p_load(brms)
library(brms)
library(rstan)
library(rethinking)
```

```{r}
CB_gender_con <- read.csv("CB_gender_con.csv", sep=";")
CB_gender_con$Gender <- as.factor(CB_gender_con$Gender)
CB_gender_con$condition <- as.factor(CB_gender_con$condition)
CB_gender_con$letter_com <- as.factor(CB_gender_con$letter_com)
CB_gender_con$aquan <- as.factor(CB_gender_con$aquan)

CB <- CB_gender_con

all_data <- read.csv("all_data_v1_1.csv", sep=";")

mean(all_data$Age1+)
mean(all_data$Age2) 

age <- (23.72+23.02)/2
summary(all_data$Age1)
summary(all_data$Age2)

age <- summary((all_data$Age1+all_data$Age2)/2)
age

summary(all_words$Age)

summary(all_words)

mean(all_words$Age, Gender=="Female")

```

```{r}
prior_CB_get <- get_prior(CB ~ condition, data = CB_gender_con, family = gaussian())
prior_CB_get


prior <-c(prior(normal(0, 10), class = "Intercept"), #no collaboratuve benefit of mixed - 
          prior(normal(0, 1), class = "b"), 
          prior(normal(0, 10), class = "sigma")) #dont expect much noise but with uncertainty 

prior_inter <- c(prior(normal(0, 10), class = "Intercept"),
          prior(normal(0, 10), class = "sigma"))

check_ <- brm(CB ~ condition,
               prior = prior,
               sample_prior = "only", #predictive prior check 
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
pp_check(check_)

dens(CB$CB, norm.comp = T)
```


```{r}
model_con_ <- brm(CB ~ condition ,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_con_)
plot(model_con_)
pp_check(model_con_)
```

```{r}
model_letter_ <- brm(CB ~ letter_com ,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_letter_)
plot(model_letter_)
pp_check(model_letter_)
```

```{r}
model_con_let_ <- brm(CB ~ condition + letter_com,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_con_let_)
plot(model_con_let_)
pp_check(model_con_let_)
```

```{r}
model_gender_ <- brm(CB ~ Gender,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_gender_)
plot(model_gender_)
pp_check(model_gender_)
```

```{r}
# model_aq_ <- brm(CB ~ aquan,
#                prior = prior,
#                data= CB,
#                family=gaussian(), 
#                iter = 5000, warmup = 2000, cores = 2)
# summary(model_aq_)
# plot(model_aq_)
# pp_check(model_aq_)
```

```{r}
CB$aquan <- as.numeric(CB$aquan)

model_con_aq_ <- brm(CB ~ condition + aquan,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_con_aq_)
plot(model_con_aq_)
pp_check(model_con_aq_)

# Population-Level Effects: 
#                      Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
# Intercept                3.22      1.09     1.12     5.34      19839 1.00
# conditionScore.SameF     0.29      0.93    -1.52     2.15      20920 1.00
# conditionScore.SameM     0.16      0.90    -1.60     1.95      20070 1.00
# aquan2                  -0.60      0.91    -2.41     1.18      20233 1.00
# aquan3                  -0.49      0.92    -2.27     1.30      20898 1.00
# aquan4                   1.27      0.95    -0.62     3.14      22114 1.00
# aquan5                  -0.19      0.96    -2.06     1.71      20695 1.00
```

```{r}
CB$trial <- as.factor(CB$trial)
model_trial_ <- brm(CB ~ trial,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_trial_)
plot(model_trial_)
pp_check(model_trial_)
```

```{r}

model_null_ <- brm(CB ~ 1,
               prior = prior_inter,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_null_)
plot(model_null_)
pp_check(model_null_)
```

```{r}

model_inter_con_let <- brm(CB ~ condition * letter_com,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_inter_con_let)
plot(model_inter_con_let)
pp_check(model_inter_con_let)
```

```{r}
model_inter_con_aq <- brm(CB ~ condition * aquan,
               prior = prior,
               data= CB,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_inter_con_aq)
plot(model_inter_con_aq)
pp_check(model_inter_con_aq)
```


```{r}
waic_con <- waic(model_con_)
waic_let <- waic(model_letter_)
waic_con_let <- waic(model_con_let_)
waic_gen <- waic(model_gender_)
waic_aq <- waic(model_aq_)
waic_con_aq <- waic(model_con_aq_)
waic_trial <- waic(model_trial_)
waic_null <- waic(model_null_)
waic_inter_con <- waic(model_inter_con_let)
waic_inter_con_aq <- waic(model_inter_con_aq)

waic_same <- waic(model_ff)


loo_compare(waic_con, waic_let, waic_con_let, waic_gen, waic_con_aq, waic_trial, waic_null, waic_inter_con, waic_inter_con_aq, waic_same)
compare_ic(waic_con, waic_let, waic_con_let, waic_gen, waic_con_aq, waic_trial, waic_null, waic_inter_con, waic_inter_con_aq)

```

```{r}
ggplot(CB, aes(y=CB, x=condition, colour=condition))+
  geom_bar(stat = "summary") + 
  geom_errorbar(stat="summary", fun.data=mean_se, width=0.3)
```

```{r}
ggplot(CB, aes(y=CB, x=letter_com, fill=letter_com))+
  geom_bar(stat = "summary") + 
  geom_errorbar(stat="summary", fun.data=mean_se, width=0.3) #SANDARD ERROR - BARS 
```
```{r}
script$condition <- as.factor(script$condition)

ggplot(script, aes(y=score, x=condition, colour=condition)) +
  geom_boxplot()
```

```{r}
score_same <- read.csv("CB_scoreSame.csv", sep=";")
score_same$condition <- as.factor(score_same$condition)

ggplot(score_same, aes(y=CB, x=condition, colour=condition)) +
  geom_boxplot()
```
```{r}
ggplot(score_same, aes(y=CB, x=condition, colour=condition))+
  geom_bar(stat = "summary") + 
  geom_errorbar(stat="summary", fun.data=mean_se, width=0.3)
```


```{r}
model_ff <- brm(CB ~ condition,
               prior = prior,
               data= score_same,
               family=gaussian(), 
               iter = 5000, warmup = 2000, cores = 2)
summary(model_ff)
plot(model_ff)
pp_check(model_ff)
```

